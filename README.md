
<div align="center">
<img src="https://gitlab.com/uploads/-/system/project/avatar/14122118/pablo.jpg?width=88"/>
<h1>Welcome to Pablo the Plugin !</h1>

[![pipeline status](https://gitlab.com/tresorio/platform/pablo/badges/dev/pipeline.svg)](https://gitlab.com/tresorio/platform/pablo/commits/dev)
[![coverage report](https://gitlab.com/tresorio/platform/pablo/badges/dev/coverage.svg)](https://gitlab.com/tresorio/platform/pablo/commits/dev)

</div>


<h2>Release</h2>
<p>Every plugin release has to follow the semantic versioning norm as described <a href="https://semver.org/">here.</a></p>